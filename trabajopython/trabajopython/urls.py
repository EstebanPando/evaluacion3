"""trabajopython URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^', include('home.urls', namespace='home')),    

    #URL de producto
    url(r'^', include('producto.urls', namespace="producto")),

    #URL de adquisiciones
    url(r'^', include('adquisiciones.urls', namespace="adquisiciones")),

    #URL de proveedor
    url(r'^', include('proveedor.urls', namespace="proveedor")),
    
    #URL de organizacion
    url(r'^', include('organizacion.urls', namespace="organizacion")),

    #URL de inventario
    url(r'^', include('inventario.urls', namespace="inventario")),

    #URLS de contabilidad
    url(r'^', include('contabilidad.urls', namespace="cuenta")),
    url(r'^', include('contabilidad.urls', namespace="traspaso")),
        url(r'^', include('contabilidad.urls', namespace="contabilidad")),

    #URL de bodega
    url(r'^', include('bodega.urls', namespace="bodega")),
    url(r'^', include('bodega.urls', namespace="ubicacion")),
]
