from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import *
from .models import *
from .urls import *
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Table, TableStyle, Image
from reportlab.lib.enums import TA_CENTER
from reportlab.lib import colors
from django.views.generic import View
from django.http import HttpResponse
from django.db.models import Count
# Create your views here.

class TraspasoDineroList(ListView):
    model = TraspasoDinero
    template_name = 'contabilidad/traspaso_list.html'

class TraspasoDineroCreate(CreateView):
    model = TraspasoDinero
    form_class = TraspasoDineroForm
    template_name = 'contabilidad/traspaso_create.html'
    success_url = reverse_lazy('traspaso:traspaso_list')
    success_message = 'Traspaso aprovado'

class TraspasoDineroUpdate(UpdateView):
    model = TraspasoDinero
    form_class = TraspasoDineroForm
    template_name = 'contabilidad/traspaso_update.html'
    success_url = reverse_lazy('traspaso:traspaso_list')


class TraspasoDineroDelete(DeleteView):
    model = TraspasoDinero
    template_name = 'contabilidad/traspaso_delete.html'
    success_url = reverse_lazy('traspaso:traspaso_list')

class CuentaList(ListView):
    model = Cuenta
    template_name = 'cuenta/cuenta_list.html'

class CuentaCreate(CreateView):
    model = Cuenta
    form_class = CuentaForm
    template_name = 'cuenta/cuenta_create.html'
    success_url = reverse_lazy('cuenta:cuenta_list')
    success_message = 'Cuenta creada'

class CuentaUpdate(UpdateView):
    model = Cuenta
    form_class = CuentaForm
    template_name = 'cuenta/cuenta_update.html'
    success_url = reverse_lazy('cuenta:cuenta_list')


class CuentaDelete(DeleteView):
    model = Cuenta
    template_name = 'cuenta/cuenta_delete.html'
    success_url = reverse_lazy('cuenta:cuenta_list')


class ReporteContabilidadPDF(View):
    def cabecera(self,pdf):
        #archivo_imagen = settings.STATIC_ROOT+'/img/escudo.png'
        #pdf.drawImage(archivo_imagen, 50, 740, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(220, 790, u"Contabilidad")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"Reporte de traspasos")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        encabezados = ('Cuenta Origen', 'Cuenta Destino', 'Fecha', 'Monto')
        detalles = [(con.cuenta_origen.tipo_cuenta, con.cuenta_destino, con.fecha, con.monto)for con in TraspasoDinero.objects.all()]
        detalle_orden = Table([encabezados] + detalles, colWidths=[4 * cm, 3 * cm, 3 * cm, 3 * cm])
        detalle_orden.setStyle(TableStyle(
        [
            ('ALIGN', (0,0),(3,0), 'CENTER'),
            ('GRID', (0,0), (-1,-1), 1, colors.black),
            ('FONTSIZE', (0,0), (-1,-1), 10),
            ]
        ))
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 60, y)
    template_name = 'contabilidad/reporteContabilidad.html'

class ReporteFondosPDF(View):
    def cabecera(self,pdf):
        #archivo_imagen = settings.STATIC_ROOT+'/img/escudo.png'
        #pdf.drawImage(archivo_imagen, 50, 740, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(245, 790, u"Contabilidad")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"Reporte de Fondos")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        encabezados = ('Cuenta Origen','Monto')
        #detalles= [ (c, TraspasoDinero.objects.filter( monto=c.id ).count()) for c in TraspasoDinero.objects.all() ]
        detalles = [(TraspasoDinero.cuenta_origen, TraspasoDinero.monto)for TraspasoDinero in TraspasoDinero.objects.all()]
        detalle_orden = Table([encabezados] + detalles, colWidths=[3 * cm, 3 * cm])
        detalle_orden.setStyle(TableStyle(
            [
                ('ALIGN', (0, 0), (1, 0), 'CENTER'),
                ('GRID', (0, 0), (8, -1), 0.5, colors.darkgrey),
                ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                ('FONTSIZE', (0, 0), (-1, -1), 8)

            ]
        ))
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 60, y)

    template_name = 'contabilidad/reporteFondos.html'