from django.contrib import admin

# Register your models here.
from .models import *


class AdminCuenta(admin.ModelAdmin):
    list_display = ['tipo_cuenta','fondos_actuales','fondos_minimos']

class AdminTraspaso(admin.ModelAdmin):
    list_display = ['id','cuenta_origen','monto','fecha']


admin.site.register(Cuenta,AdminCuenta)
admin.site.register(TraspasoDinero,AdminTraspaso)