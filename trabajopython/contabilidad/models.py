from django.db import models

# Create your models here.



class Cuenta(models.Model):
    tipo_cuenta = models.CharField(verbose_name='Tipo de cuenta', max_length=100,
                                   choices= ( ('1.- Computacion','Computacion'),('2.- Vehiculos','Vehiculos'),('3.- Inmuebles','Inmuebles')), unique = True)
    fondos_actuales = models.IntegerField(verbose_name='Fondos actuales')
    fondos_minimos = models.IntegerField(verbose_name='Fondos minimos')

    def __str__(self):
        return self.tipo_cuenta

    class Meta:
        verbose_name = "Cuenta"


class TraspasoDinero(models.Model):
    cuenta_origen = models.ForeignKey(Cuenta)
    cuenta_destino = models.CharField(verbose_name='Tipo de cuenta', max_length=100,
                                   choices= ( ('1','Computacion'),('2','Vehiculos'),('3','Inmuebles')))
    fecha = models.DateField(auto_now=False, auto_now_add=False)
    monto = models.IntegerField()

    def __int__(self):
        return self.id
    class Meta:
        verbose_name = "Traspaso de dinero"

