from django import forms

from .models import TraspasoDinero
from .models import Cuenta


class TraspasoDineroForm(forms.ModelForm):
    class Meta:
        model = TraspasoDinero

        fields = [
            'cuenta_origen',
            'cuenta_destino',
            'fecha',
            'monto',
        ]

        labels = {
            'cuenta_origen': 'Cuenta de origen',
            'cuenta_destino': 'cuenta de destino',
            'fecha': 'Fecha',
            'monto': 'Monto',
        }

        widgets = {
            'cuenta_origen': forms.Select(attrs={'class': 'form-control'}),
            'cuenta_destino': forms.Select(attrs={'class': 'form-control'}),
            'fecha': forms.SelectDateWidget(),
            'monto': forms.NumberInput(attrs={'class': 'form-control'}),
        }
class CuentaForm(forms.ModelForm):
    class Meta:
        model = Cuenta

        fields = [
            'tipo_cuenta',
            'fondos_actuales',
            'fondos_minimos',
        ]
        labels = {
            'tipo_cuenta': 'Tipo de cuenta',
            'fondos_actuales': 'Fondos actuales',
            'fondos_minimos': 'Fondos minimos',
        }
        widgets = {
            'tipo_cuenta': forms.Select(attrs={'class': 'form-control'}),
            'fondos_actuales': forms.NumberInput(attrs={'class': 'form-control'}),
            'fondos_minimos': forms.NumberInput(attrs={'class': 'form-control'}),
        }
