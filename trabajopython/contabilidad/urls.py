from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *
from producto import views

urlpatterns = [

    # traspaso fondos
    url(r'^contabilidad/traspaso/listar', TraspasoDineroList.as_view(), name='traspaso_list'),
    url(r'^contabilidad/traspaso/crear', TraspasoDineroCreate.as_view(), name='traspaso_crear'),
    url(r'^contabilidad/traspaso/editar/(?P<pk>\d+)$', TraspasoDineroUpdate.as_view(), name='traspaso_editar'),
    url(r'^contabilidad/traspaso/eliminar/(?P<pk>\d+)$', TraspasoDineroDelete.as_view(), name='traspaso_eliminar'),

    # cuentas
    #url(r'^contabilidad/listar/cuenta', CuentaList.as_view(), name='cuenta_list'),

    #reportes
    url(r'^contabilidad/reporteContabilidad/$', ReporteContabilidadPDF.as_view(), name='reporteContabilidad'),
    url(r'^contabilidad/reporteFondos/$', ReporteFondosPDF.as_view(), name='reporteFondos'),
    # cuentas
    url(r'^contabilidad/cuenta/listar$', CuentaList.as_view(), name='cuenta_list'),
    url(r'^contabilidad/cuenta/crear$', CuentaCreate.as_view(), name='cuenta_crear'),
    url(r'^contabilidad/cuenta/editar/(?P<pk>\d+)/$', CuentaUpdate.as_view(), name='cuenta_editar'),
    url(r'^contabilidad/cuenta/eliminar/(?P<pk>\d+)/$', CuentaDelete.as_view(), name='cuenta_eliminar'),
]