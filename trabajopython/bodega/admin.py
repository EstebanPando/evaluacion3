from django.contrib import admin
from .models import *
# Register your models here.

class AdminUbicacion(admin.ModelAdmin):
    list_display = ["estante"]
    list_filter = ["estante"]
    search_fields = ["estante"]

class AdminBodega(admin.ModelAdmin):
    list_display = ["stock_bodega"]
    list_filter = ["stock_bodega"]
    search_fields = ["stock_bodega"]

admin.site.register(Ubicacion, AdminUbicacion)
admin.site.register(Bodega, AdminBodega)