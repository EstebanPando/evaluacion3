from django.db import models
from producto.models import *

class Ubicacion(models.Model):
    estante = models.IntegerField()
    pasillo = models.IntegerField()
    nivel = models.IntegerField()

    def __str__(self):
        return str("Estante " +str(self.estante) + " Pasillo " + str(self.pasillo) + " Nivel " + str(self.nivel))

    class Meta:
        verbose_name = "Ubicacion"

class Bodega(models.Model):
    producto = models.ForeignKey('producto.Producto')
    fecha_ingreso = models.DateTimeField(auto_now=False, auto_now_add=False)
    stock_bodega = models.IntegerField(default=0)
    estado = models.CharField(max_length=30,choices=(('0','En inventario'),('1','En bodega')),default=1)
    ubicacion = models.ForeignKey(Ubicacion,default=0)

    def __int__(self):
        return self.stock_bodega

    class Meta:
        verbose_name = "Bodega"
