from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import *
from .forms import *
from .models import *
from django.views.generic import View

from reportlab.platypus import TableStyle, Table, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, landscape
from django.http import HttpResponse

# Create your views here.
class BodegaCreate(CreateView):
    model = Bodega
    form_class = BodegaForm
    template_name = 'bodega/bodega_create.html'
    success_url = reverse_lazy('bodega:bodega_listar')

class BodegaList(ListView):
    model = Bodega
    form_class = BodegaForm
    template_name = 'bodega/bodega_list.html'
    success_url = reverse_lazy('bodega:bodega_listar')

class BodegaUpdate(UpdateView):
    model = Bodega
    form_class = BodegaForm
    template_name = 'bodega/bodega_update.html'
    success_url = reverse_lazy('bodega:bodega_listar')

class BodegaDelete(DeleteView):
    model = Bodega
    form_class = BodegaForm
    template_name = 'bodega/bodega_delete.html'
    success_url = reverse_lazy('bodega:bodega_listar')

class UbicacionCreate(CreateView):
    model = Ubicacion
    form_class = UbicacionForm
    template_name = 'ubicacion/ubicacion_create.html'
    success_url = reverse_lazy('ubicacion:ubicacion_listar')

class UbicacionList(ListView):
    model = Ubicacion
    form_class = UbicacionForm
    template_name = 'ubicacion/ubicacion_list.html'
    #success_url = reverse_lazy('ubicacion:ubicacion_listar')

class UbicacionUpdate(UpdateView):
    model = Ubicacion
    form_class = UbicacionForm
    template_name = 'ubicacion/ubicacion_update.html'
    success_url = reverse_lazy('ubicacion:ubicacion_listar')

class UbicacionDelete(DeleteView):
    model = Ubicacion
    form_class = UbicacionForm
    template_name = 'ubicacion/ubicacion_delete.html'
    success_url = reverse_lazy('ubicacion:ubicacion_listar')


class Reporte(View):

    def cabecera(self, pdf):
        # imagen = settings.MEDIA_ROOT+'\imagenes\conaf.jpg'
        # pdf.drawImage(imagen, 40, 520, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(300, 550, u"PRODUCTOS EN BODEGA")
        pdf.setFont("Helvetica", 10)
        # pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        # response['Content-Disposition']='attachment; filename="%s"'
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        encabezados = ('N° Bodega', 'Producto', 'stock', 'estado', 'estante', 'pasillo', 'nivel')

        detalles = [(Bodega.id, Bodega.producto, Bodega.stock_bodega, Bodega.estado,Bodega.ubicacion.estante, Bodega.ubicacion.pasillo, Bodega.ubicacion.nivel)
                    for Bodega in Bodega.objects.all()]
        detalle_orden = Table([encabezados] + detalles,
                              colWidths=[2 * cm, 13.5* cm, 3.5 * cm, 1.5 * cm, 1.5 * cm, 1.5* cm, 1.5* cm])

        detalle_orden.setStyle(TableStyle(
            [
                ('ALIGN', (0, 0), (1, 0), 'CENTER'),
                ('GRID', (0, 0), (8, -1), 0.5, colors.darkgrey),
                ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                ('FONTSIZE', (0, 0), (-1, -1), 8)

            ]
        ))

        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 40, 450)
        pdf.setPageSize(landscape(letter))

    template_name = 'bodega/reporte.html'
    # success_url = reverse_lazy('adquisiciones:ordencompra_listar')
