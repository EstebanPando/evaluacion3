from django import forms
from .models import *

class BodegaForm(forms.ModelForm):

	class Meta:

		model = Bodega

		fields = [
			'producto',
			'fecha_ingreso',
			'stock_bodega',
			'estado',
			'ubicacion',

		]
		labels = {
			'producto': 'Código y nombre de producto',
			'fecha_ingreso': 'Fecha de ingreso',
			'stock_bodega': 'Stock de producto en bodega',
            'estado' : 'Estado',
			'ubicacion': 'Ubicación de producto',
		}
		widgets = {
			'producto': forms.Select(attrs={'class': 'form-control', 'placeholder' : 'Seleccione el codigo de producto'}),
			'fecha_ingreso': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese la fecha de ingreso del producto'}),
			'stock_bodega': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese el stock del producto'}),
            'estado': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione el estado de producto'}),
            'ubicacion': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Seleccione la ubicacion de la bodega'}),
		}

class UbicacionForm(forms.ModelForm):

	class Meta:
		model = Ubicacion

		fields = [
			'estante',
			'pasillo',
			'nivel',

		]
		labels = {
			'estante': 'Número de estante',
			'pasillo': 'Número de pasillo',
			'nivel': 'Nivel estante',
		}
		widgets = {
			'estante': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese número de estante'}),
			'pasillo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese número de pasillo'}),
			'nivel': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese nivel de estante'}),
		}