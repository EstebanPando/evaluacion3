from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *
from bodega import views

urlpatterns = [
    #bodega
    url(r'^bodega/crear/bodega$', BodegaCreate.as_view(), name='bodega_crear'),
    url(r'^bodega/listar/bodega$', BodegaList.as_view(), name='bodega_listar'),
    url(r'^bodega/editar/bodega/(?P<pk>\d+)/$', BodegaUpdate.as_view(), name='bodega_editar'),
    url(r'^bodega/eliminar/bodega/(?P<pk>\d+)/$', BodegaDelete.as_view(), name='bodega_eliminar'),
    #ubicacion
    url(r'^ubicacion/crear/ubicacion$', UbicacionCreate.as_view(), name='ubicacion_crear'),
    url(r'^ubicacion/listar/ubicacion$', UbicacionList.as_view(), name='ubicacion_listar'),
    url(r'^ubicacion/editar/ubicacion/(?P<pk>\d+)/$', UbicacionUpdate.as_view(), name='ubicacion_editar'),
    url(r'^ubicacion/eliminar/ubicacion/(?P<pk>\d+)/$', UbicacionDelete.as_view(), name='ubicacion_eliminar'),

    url(r'^bodega/reporte$', Reporte.as_view(), name='reporte'),
]