from django.contrib import admin
from .models import *
# Register your models here.
class AdminOrdenCompra(admin.ModelAdmin):
    list_display = ["total", "estado"]
    #list_filter = ["total"]
    #search_fields = ["producto"]

admin.site.register(OrdenCompra, AdminOrdenCompra)

class AdminFactura(admin.ModelAdmin):
    list_display = ["proveedor","producto", "total"]
    #list_filter = ["total"]
    search_fields = ["producto"]

admin.site.register(Factura,AdminFactura)