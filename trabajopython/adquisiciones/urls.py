from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *
from producto import views


urlpatterns = [
	#OrdenCompra
	url(r'^adquisiciones/listar/ordencompra$', OrdenCompraList.as_view(), name='ordencompra_listar'),
    url(r'^adquisiciones/crear/ordencompra$', OrdenCompraCreate.as_view(), name='ordencompra_crear'),
    url(r'^adquisiciones/editar/ordencompra/(?P<pk>\d+)/$', OrdenCompraUpdate.as_view(), name='ordencompra_editar'),
    url(r'^adquisiciones/eliminar/ordencompra/(?P<pk>\d+)/$', OrdenCompraDelete.as_view(), name='ordencompra_eliminar'),
    url(r'^adquisiciones/reporte$', Reporte.as_view(), name='reporte'),
    #Factura
    url(r'^adquisiciones/listar/factura$', FacturaList.as_view(), name='factura_listar'),
    url(r'^adquisiciones/crear/factura$', FacturaCreate.as_view(), name='factura_crear'),
    url(r'^adquisiciones/editar/factura/(?P<pk>\d+)/$', FacturaUpdate.as_view(), name='factura_editar'),
    url(r'^adquisiciones/eliminar/factura/(?P<pk>\d+)/$', FacturaDelete.as_view(), name='factura_eliminar'),
]