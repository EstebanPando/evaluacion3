from django import forms
from .models import *
from datetimewidget.widgets import DateWidget

class OrdenCompraForm(forms.ModelForm):

	class Meta:
		model = OrdenCompra

		fields = [
			'producto',
			'total',
			'estado',
			'observacion',
			'departamento',
			'fecha',
		]
		labels = {
			'producto': 'Producto',
			'total': 'Total',
			'estado': 'Estado',
			'observacion': 'Observacion',
			'departamento': 'Departamento',
			'fecha': 'Fecha',
		}
		widgets = {
			'producto': forms.Select(attrs={'class': 'form-control'}),
			'total': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese total'}),
			'estado': forms.Select(attrs={'class': 'form-control'}),
			'observacion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ingrese observacion', 'rows': 3}),
			'departamento': forms.Select(attrs={'class': 'form-control'}),
			'fecha': DateWidget(attrs={'id': "fecha"}, usel10n=True, bootstrap_version=3),
		}

class FacturaForm(forms.ModelForm):

	class Meta:
		model = Factura

		fields = [
			'producto',
			'proveedor',
			'fecha',
			'total',
			
		]
		labels = {
			'producto': 'Producto',
			'proveedor': 'Proveedor',
			'fecha': 'Fecha',
			'total': 'Total',
		}
		widgets = {
			'producto': forms.Select(attrs={'class': 'form-control'}),			
			'proveedor': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Ingrese proveedor'}),
			'fecha': DateWidget(attrs={'id': "fecha"}, usel10n=True, bootstrap_version=3),
			'total': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese total'}),
		}