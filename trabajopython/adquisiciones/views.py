from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import *
from .forms import *
from .models import *
from django.views.generic import View

from reportlab.platypus import TableStyle, Table, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, landscape
from django.http import HttpResponse
# Create your views here.

class OrdenCompraList(ListView):
    model = OrdenCompra
    template_name = 'adquisiciones/ordencompra_list.html'


class OrdenCompraCreate(CreateView):
    model = OrdenCompra
    form_class = OrdenCompraForm
    template_name = 'adquisiciones/ordencompra_create.html'
    success_url = reverse_lazy('adquisiciones:ordencompra_listar')


class OrdenCompraUpdate(UpdateView):
    model = OrdenCompra
    form_class = OrdenCompraForm
    template_name = 'adquisiciones/ordencompra_update.html'
    success_url = reverse_lazy('adquisiciones:ordencompra_listar')


class OrdenCompraDelete(DeleteView):
    model = OrdenCompra
    template_name = 'adquisiciones/ordencompra_delete.html'
    success_url = reverse_lazy('adquisiciones:ordencompra_listar')


class Reporte(View):

    def cabecera(self, pdf):
        #imagen = settings.MEDIA_ROOT+'\imagenes\conaf.jpg'
        #pdf.drawImage(imagen, 40, 520, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(300, 550, u"ORDEN DE COMPRA")
        pdf.setFont("Helvetica", 10)
        #pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        #response['Content-Disposition']='attachment; filename="%s"'
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        contador = OrdenCompra.objects.values('departamento_id').count

        encabezados = ('N° Orden de Compra', 'Producto', 'total', 'estado', 'observacion', 'departamento', 'fecha')

        detalles = [(OrdenCompra.id, OrdenCompra.producto, OrdenCompra.total, OrdenCompra.estado, OrdenCompra.observacion, OrdenCompra.departamento, OrdenCompra.fecha)
                    for OrdenCompra in OrdenCompra.objects.all()]
        detalle_orden = Table([encabezados] + detalles, colWidths=[3 * cm, 5.5 * cm, 2 * cm, 1.2 * cm, 5 * cm, 3 * cm, 3 * cm])

        detalle_orden.setStyle(TableStyle(
            [
                ('ALIGN', (0, 0), (1, 0), 'CENTER'),
                ('GRID', (0, 0), (8, -1), 0.5, colors.darkgrey),
                ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                ('FONTSIZE', (0, 0), (-1, -1), 8)
                
            ]
        ))

        
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 40, 450)
        pdf.setPageSize(landscape(letter))

    template_name = 'adquisiciones/reporte.html'
    #success_url = reverse_lazy('adquisiciones:ordencompra_listar')

class FacturaList(ListView):
    model = Factura
    template_name = 'adquisiciones/factura_list.html'


class FacturaCreate(CreateView):
    model = Factura
    form_class = FacturaForm
    template_name = 'adquisiciones/factura_create.html'
    success_url = reverse_lazy('adquisiciones:factura_listar')


class FacturaUpdate(UpdateView):
    model = Factura
    form_class = FacturaForm
    template_name = 'adquisiciones/factura_update.html'
    success_url = reverse_lazy('adquisiciones:factura_listar')


class FacturaDelete(DeleteView):
    model = Factura
    template_name = 'adquisiciones/factura_delete.html'
    success_url = reverse_lazy('adquisiciones:factura_listar')
