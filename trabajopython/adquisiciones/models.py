from django.db import models
from producto.models import Producto
from proveedor.models import Proveedor
from organizacion.models import Departamento
#from contabilidad.models import Cuenta

# Create your models here.

class OrdenCompra(models.Model):
    producto = models.ForeignKey(Producto, default="")
    total = models.IntegerField(blank=False, null=False, verbose_name="Precio")
    estado = models.CharField(max_length=20, choices=(('1', 'Aceptado'), ('0', 'Rechazado'), ('2', 'En Proceso')), verbose_name="Estado")
    observacion = models.CharField(max_length=200, verbose_name="Observacion")
    departamento = models.ForeignKey(Departamento)
    fecha = models.DateField(auto_now=False, auto_now_add=False)
    #cuenta=models.ForeignKey(Cuenta)

    def __int__(self):
        return self.id

    class Meta:
        verbose_name="Orden de Compra"

class Factura(models.Model):
    producto = models.ForeignKey(Producto)
    proveedor = models.ForeignKey(Proveedor)
    fecha = models.DateField(auto_now=False, auto_now_add=False, verbose_name="Fecha")
    total = models.IntegerField(blank=False, null=False, verbose_name="Total")

    def __int__(self):
        return self.id

    class Meta:
        verbose_name="Factura"
