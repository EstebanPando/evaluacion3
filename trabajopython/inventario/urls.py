from django.conf.urls import url
from .views import *
from django.contrib import admin

urlpatterns = [
    url(r'^inventario/listar/inventario', ListInventario.as_view(), name='listar_inventario'),
    url(r'^inventario/crear/inventario', CreateInventario.as_view(), name='crear_inventario'),
    url(r'^inventario/editar/inventario/(?P<pk>\d+)$', UpdateInventario.as_view(), name='editar_inventario'),
    url(r'^inventario/eliminar/inventario/(?P<pk>\d+)$', DeleteInventario.as_view(), name='eliminar_inventario'),
    url(r'^inventario/reporte$', ReporteInventario.as_view(), name='reporte_inventario'),
]
