from django.contrib import admin
from .models import *
# Register your models here.


class AdminInventario(admin.ModelAdmin):
    list_display = ["id"]
    list_filter = ["id"]
    search_fields = ["id"]
admin.site.register(Inventario, AdminInventario)
