from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail
from .forms import *
from django.views.generic import CreateView,DeleteView,ListView,UpdateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.db.models import Q
from django.views.generic import View
from django.http import HttpResponse
from reportlab.platypus import TableStyle, Table, Spacer
from reportlab.lib import colors
from reportlab.lib.units import cm
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, landscape
# Create your views here.


class CreateInventario(CreateView):
    model = Inventario
    template_name = 'Inventario/crear_inventario.html'
    form_class = InventarioForm
    success_url = reverse_lazy('inventario:listar_inventario')


class UpdateInventario(UpdateView):
    model = Inventario
    form_class = InventarioForm
    template_name = 'inventario/editar_inventario.html'
    success_url = reverse_lazy('inventario:listar_inventario')


class DeleteInventario(DeleteView):
    model = Inventario
    template_name = 'inventario/eliminar_inventario.html'
    success_url = reverse_lazy('inventario:listar_inventario')
    success_message = 'Inventario eliminado correctamente'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteInventario, self).delete(request, *args, **kwargs)


class ListInventario(ListView):
    model = Inventario
    template_name = 'inventario/listar_inventario.html'
    paginate_by = 20
    #queryset = (Inventario.objects.all())

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(departamento__codigo_departamento__icontains=search)

            return Inventario.objects.filter(qs)

        return Inventario.objects.get_queryset()

class ReporteInventario(View):

    def cabecera(self, pdf):
        #imagen = settings.MEDIA_ROOT+'\imagenes\conaf.jpg'
        #pdf.drawImage(imagen, 40, 520, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(300, 550, u"Inventario")
        pdf.setFont("Helvetica", 10)
        #pdf.drawString(200, 770, u"")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        #response['Content-Disposition']='attachment; filename="%s"'
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        encabezados = ('Departamento', 'Producto')

        detalles = [(inventario.departamento, inventario.producto)
                    for inventario in Inventario.objects.all()]
        detalle_orden = Table([encabezados] + detalles, colWidths=[5 * cm, 8 * cm])

        detalle_orden.setStyle(TableStyle(
            [
                ('ALIGN', (0, 0), (1, 0), 'CENTER'),
                ('GRID', (0, 0), (8, -1), 0.5, colors.darkgrey),
                ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                ('FONTSIZE', (0, 0), (-1, -1), 8)
                
            ]
        ))

        
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 40, 450)
        pdf.setPageSize(landscape(letter))

    template_name = 'inventario/reporte_inventario.html'