from django import forms

from .models import *


class InventarioForm(forms.ModelForm):

    class Meta:
        model = Inventario

        fields = [
            #'codigo_inventario',
            'departamento',
            'producto',
        ]

        labels = {
            #'codigo_inventario':'Codigo Inventario',
            'departamento':'Departamento',
            'producto':'Producto',
        }

        widgets = {
            #'codigo_inventario':forms.NumberInput(attrs={'class':'form-control'}),
            'departamento':forms.Select(attrs={'class':'form-control', 'placeholder':'Ingrese Nombre Departamento'}),
            'producto':forms.Select(attrs={'class':'form-control', 'placeholder':'Ingrese Bodega'}),
        }
