from django.conf.urls import url
from .views import HomeTemplateView


urlpatterns = [
    url(r'^$', HomeTemplateView.as_view(), name='index'),
]
