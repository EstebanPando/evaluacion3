from django.contrib import admin
from .models import *
# Register your models here.


class AdminOrganizacion(admin.ModelAdmin):
    list_display = ["codigo_organizacion"]
    list_filter = ["codigo_organizacion"]
    search_fields = ["codigo_organizacion"]


class AdminDireccion(admin.ModelAdmin):
    list_display = ["codigo_direccion"]
    list_filter = ["codigo_direccion"]
    search_fields = ["codigo_direccion"]


class AdminDepartamento(admin.ModelAdmin):
    list_display = ["codigo_departamento"]
    list_filter = ["codigo_departamento"]
    search_fields = ["codigo_departamento"]


admin.site.register(Organizacion, AdminOrganizacion)
admin.site.register(Direccion, AdminDireccion)
admin.site.register(Departamento, AdminDepartamento)
