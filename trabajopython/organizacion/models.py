from django.db import models

# Create your models here.

class Organizacion(models.Model):

    codigo_organizacion = models.CharField(max_length=20)
    nombre_organizacion = models.CharField(max_length=20)

    def __str__(self):
        return self.codigo_organizacion

    class Meta:
        verbose_name ="Organizacion"

class Direccion(models.Model):
    organizacion = models.ForeignKey(Organizacion)
    codigo_direccion = models.CharField(max_length=20)
    nombre_direccion = models.CharField(max_length=20)

    def __str__(self):
        return str(str(self.organizacion)+ self.codigo_direccion)

    class Meta:
        verbose_name ="Direccion"


class Departamento(models.Model):
    direccion = models.ForeignKey(Direccion)
    codigo_departamento = models.CharField(max_length=20)
    nombre_departamento = models.CharField(max_length=20)

    def __str__(self):
        return str(str(self.direccion) +self.codigo_departamento )

    class Meta:
        verbose_name = "Departamento"
