from django.conf.urls import url
from .views import *
from django.contrib import admin

urlpatterns = [
    url(r'^organizacion/listar/direccion', ListDireccion.as_view(), name='listar_direccion'),
    url(r'^organizacion/listar/departamento', ListDepartamento.as_view(), name='listar_departamento'),
    url(r'^organizacion/listar/oraganizacion', ListOrganizacion.as_view(), name='listar_organizacion'),
    url(r'^organizacion/crear/departamento', CreateDepartamento.as_view(), name='crear_departamento'),
    url(r'^organizacion/crear/direccion', CreateDireccion.as_view(), name='crear_direccion'),
    url(r'^organizacion3/crear/organizacion', CreateOrganizacion.as_view(), name='crear_organizacion'),
]