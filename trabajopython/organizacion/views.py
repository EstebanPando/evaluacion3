from django.shortcuts import render
from .forms import *
from django.views.generic import CreateView,DeleteView,ListView,UpdateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.db.models import Q
# Create your views here.


class CreateDireccion(CreateView):
    model = Direccion
    template_name = 'organizacion/crear_direccion.html'
    form_class = DireccionForm
    success_url = reverse_lazy('organizacion:listar_direccion')


class ListDireccion(ListView):
    model = Direccion
    template_name = 'organizacion/listar_direccion.html'
    paginate_by = 20

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(organizacion__icontains=search)

            return Direccion.objects.filter(qs)

        return Direccion.objects.get_queryset()


class CreateOrganizacion(CreateView):
    model = Organizacion
    template_name = 'organizacion/crear_organizacion.html'
    form_class = OrganizacionForm
    success_url = reverse_lazy('organizacion:listar_organizacion')


class ListOrganizacion(ListView):
    model = Organizacion
    template_name = 'organizacion/listar_organizacion.html'
    paginate_by = 20

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(codigo_organizacion__icontains=search)

            return Organizacion.objects.filter(qs)

        return Organizacion.objects.get_queryset()


class CreateDepartamento(CreateView):
    model = Departamento
    template_name = 'organizacion/crear_departamento.html'
    form_class = DepartamentoForm
    success_url = reverse_lazy('organizacion:listar_departamento')


class ListDepartamento(ListView):
    model = Departamento
    template_name = 'organizacion/listar_departamento.html'
    paginate_by = 20

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(codigo_departamento__icontains=search)

            return Departamento.objects.filter(qs)

        return Departamento.objects.get_queryset()
