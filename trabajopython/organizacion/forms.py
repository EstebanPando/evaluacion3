from django import forms

from .models import *


class DireccionForm(forms.ModelForm):

    class Meta:
        model = Direccion

        fields = [
            'organizacion',
            'codigo_direccion',
            'nombre_direccion',
        ]

        labels = {
            'organizacion':'Organizacion',
            'codigo_direccion':'Codigo Direccion',
            'nombre_direccion':'Nombre Direccion',
        }

        widgets = {
            'organizacion':forms.Select(attrs={'class':'form-control', 'placeholder':'Organizacion'}),
            'codigo_direccion':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Cod. Direccion'}),
            'nombre_direccion':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre Direccion'}),
        }


class OrganizacionForm(forms.ModelForm):

    class Meta:
        model = Organizacion

        fields = [
            'codigo_organizacion',
            'nombre_organizacion',
        ]

        labels = {
            'codigo_organizacion':'Codigo Organizacion',
            'nombre_organizacion':'Nombre Organizacion',
        }

        widgets = {
            'codigo_organizacion':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Codigo Organizacion'}),
            'nombre_organizacion':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre Direccion'}),
        }


class DepartamentoForm(forms.ModelForm):

    class Meta:
        model = Departamento

        fields = [
            'direccion',
            'codigo_departamento',
            'nombre_departamento',
        ]

        labels = {
            'direccion':'direccion',
            'codigo_departamento':'Codigo Departamento',
            'nombre_departamento':'Nombre Departamento',
        }

        widgets = {
            'direccion':forms.Select(attrs={'class':'form-control', 'placeholder':'Direccion'}),
            'codigo_departamento':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Codigo Departamento'}),
            'nombre_departamento':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre Departamento'}),
        }