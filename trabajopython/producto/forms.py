from django import forms
from .models import *


class ProductoForm(forms.ModelForm):

	class Meta:
		model = Producto

		fields = [
			'codigo',
			'nombre_producto',
			'categoria',
			'stock_minimo',
			'marca_producto',
			'modelo_producto',
		]
		labels = {
			'codigo': 'Codigo',
			'nombre_producto': 'Nombre',
			'categoria': 'Categoria',
			'stock_minimo': 'Stock Minimo',
			'marca_producto': 'Marca',
			'modelo_producto': 'Modelo',
		}
		widgets = {
			'codigo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese codigo'}),
			'nombre_producto': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese nombre'}),
			'categoria': forms.Select(attrs={'class': 'form-control'}),
			'stock_minimo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ingrese stock minimo'}),
			'marca_producto': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese marca del producto'}),
			'modelo_producto': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese modelo del producto'}),
		}