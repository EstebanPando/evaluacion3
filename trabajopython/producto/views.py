from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import *
from .forms import *
from .models import *
from django.views.generic import View

# Create your views here.

class ProductoList(ListView):
    model = Producto
    template_name = 'producto/producto_list.html'
    #required_permissions = ('producto.add_producto',)


class ProductoCreate(CreateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'producto/producto_create.html'
    success_url = reverse_lazy('producto:producto_listar')
    success_message = 'producto creado exitosamente'
    #required_permissions = ('producto.add_producto',)


class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'producto/producto_update.html'
    success_url = reverse_lazy('producto:producto_listar')
   # required_permissions = ('producto.change_producto',)


class ProductoDelete(DeleteView):
    model = Producto
    template_name = 'producto/producto_delete.html'
    success_url = reverse_lazy('producto:producto_listar')
    required_permissions = ('producto.delete_producto',)
