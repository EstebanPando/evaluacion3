from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *
from producto import views


urlpatterns = [
	#producto
	url(r'^producto/listar/producto$', ProductoList.as_view(), name='producto_listar'),
    url(r'^producto/crear/producto$', ProductoCreate.as_view(), name='producto_crear'),
    url(r'^producto/editar/producto/(?P<pk>\d+)/$', ProductoUpdate.as_view(), name='producto_editar'),
    url(r'^producto/eliminar/producto/(?P<pk>\d+)/$', ProductoDelete.as_view(), name='producto_eliminar'),
]