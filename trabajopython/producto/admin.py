from django.contrib import admin

from .models import *
# Register your models here.
class AdminProducto(admin.ModelAdmin):
    list_display = ["codigo","nombre_producto"]
    #list_filter = ["codigo_inventario"]
    search_fields = ["nombre_producto"]


admin.site.register(Producto,AdminProducto)