from django.db import models


class Producto(models.Model):
    codigo = models.CharField(unique=True,max_length=50,verbose_name="Código Producto")
    nombre_producto = models.CharField(max_length=50,verbose_name="Nombre Producto")
    categoria = models.CharField(max_length=50,choices=(('1','Vehiculo'),('2','Inmuebles'),('3','Computacion')),verbose_name="Categoria", default=0)
    stock_minimo = models.CharField(max_length=50,verbose_name="Stock Minimo de Producto",default=1)
    marca_producto = models.CharField(max_length=50,verbose_name="Marca")
    modelo_producto = models.CharField(max_length=50,verbose_name="Modelo Producto")


    def __str__(self):
       return str("Código: " + str(self.codigo) + " | Nombre: " + str(self.nombre_producto))

    class Meta:
        verbose_name ="Producto"
