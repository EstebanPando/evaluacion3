from django.contrib import admin

from .models import *
# Register your models here.
class AdminProveedor(admin.ModelAdmin):
    list_display = ["rut","nombre_proveedor"]
    #list_filter = ["codigo_inventario"]
    search_fields = ["direccion"]


admin.site.register(Proveedor,AdminProveedor)