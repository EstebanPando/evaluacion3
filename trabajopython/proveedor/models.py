from django.db import models

# Create your models here.

class Proveedor (models.Model):
    nombre_proveedor = models.CharField(max_length=100, verbose_name="Nombre de Proveedor")
    direccion = models.CharField(max_length=100, verbose_name="Direccion")
    telefono = models.CharField(max_length=9, verbose_name="Telefono")
    rut = models.CharField(max_length=12, verbose_name="RUT")

    def __str__(self):
        return self.nombre_proveedor

    class Meta:
        verbose_name = "Proveedor"
