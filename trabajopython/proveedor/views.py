from django.shortcuts import render
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import *
from .forms import *
from .models import *
from django.views.generic import View

# Create your views here.

class ProveedorList(ListView):
    model = Proveedor
    template_name = 'proveedor/proveedor_list.html'


class ProveedorCreate(CreateView):
    model = Proveedor
    form_class = ProveedorForm
    template_name = 'proveedor/proveedor_create.html'
    success_url = reverse_lazy('proveedor:proveedor_listar')


class ProveedorUpdate(UpdateView):
    model = Proveedor
    form_class = ProveedorForm
    template_name = 'proveedor/proveedor_update.html'
    success_url = reverse_lazy('proveedor:proveedor_listar')


class ProveedorDelete(DeleteView):
    model = Proveedor
    template_name = 'proveedor/proveedor_delete.html'
    success_url = reverse_lazy('proveedor:proveedor_listar')