from django import forms
from .models import *


class ProveedorForm(forms.ModelForm):

	class Meta:
		model = Proveedor

		fields = [
			'nombre_proveedor',
			'direccion',
			'telefono',
			'rut',
		]
		labels = {
			'nombre_proveedor': 'Nombre del Proveedor',
			'direccion': 'Direccion',
			'telefono': 'Telefono',
			'rut': 'Rut',
		}
		widgets = {
			'nombre_proveedor': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese nombre del proveedor'}),
			'direccion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese direccion'}),
			'telefono': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingrese telefono'}),
			'rut': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ingrese rut'}),
		}