from django.conf.urls import url, include
from django.contrib import admin
from .models import *
from .views import *
from .models import *
from producto import views


urlpatterns = [
	#producto
	url(r'^proveedor/listar/proveedor$', ProveedorList.as_view(), name='proveedor_listar'),
    url(r'^proveedor/crear/proveedor$', ProveedorCreate.as_view(), name='proveedor_crear'),
    url(r'^proveedor/editar/proveedor/(?P<pk>\d+)/$', ProveedorUpdate.as_view(), name='proveedor_editar'),
    url(r'^proveedor/eliminar/proveedor/(?P<pk>\d+)/$', ProveedorDelete.as_view(), name='proveedor_eliminar'),
]